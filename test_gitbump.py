import subprocess
import pytest
import gitbump

@pytest.fixture
def git_mock(mocker):
    check_output_mock = mocker.patch.object(subprocess, 'check_output', autospec=True)
    return check_output_mock

def test_git_commands(git_mock):
    gitbump.git('tag', '1234')
    git_mock.assert_called_once_with(['git', 'tag', '1234'])

@pytest.mark.parametrize("message,expected", [
    # No commands
    ("""commit 96af8c626a64f150c68ac8a0ff1a0bf6bf82c060
Author: Łukasz Kwiek <lukasz.kwiek@somewhere.com>
Date:   Fri Apr 19 05:54:18 2019 +0000

Update README.md""".encode(),
    []),
    # list all commands
    ("""commit 96af8c626a64f150c68ac8a0ff1a0bf6bf82c060
Author: Łukasz Kwiek <lukasz.kwiek@somewhere.com>
Date:   Fri Apr 19 05:54:18 2019 +0000

    Update README.md

    !bump: minor, !bump: major, !bump: patch
    """.encode(),
    ['minor','major','patch']),
    # list only one
    ("""commit 96af8c626a64f150c68ac8a0ff1a0bf6bf82c060
Author: Łukasz Kwiek <lukasz.kwiek@somewhere.com>
Date:   Fri Apr 19 05:54:18 2019 +0000

    Update README.md

    !bump: minor

    Change-Id: aaaaa
    """.encode(),
    ['minor'])
    ])
def test_check_commands(git_mock, message, expected):
    git_mock.return_value = message
    assert gitbump.check_commit_msg() == expected

@pytest.mark.parametrize("version,cmds,expected", [
    # No commands
    ("1.0.0", [], "1.0.1"),
    # Minor bump
    ("0.0.3", ['minor'], "0.1.0"),
    # Major and minor bump
    ("1.0.0", ['major', 'minor'], '2.1.0')
])
def test_bump(git_mock, version, cmds, expected):
    assert gitbump.bump(version, cmds) == expected

@pytest.mark.parametrize("url,expected", [
    # Gitlab
    ("https://gitlab-ci-token:xxxxxxxxxxxxxxxxxxxx@gitlab.com/w00/gitbump.git".encode(),
     "git@gitlab.com:w00/gitbump.git"),
    # SSH url
    ("ssh://user@gerrit.some.com:29418/proj/repo".encode(),
     "ssh://user@gerrit.some.com:29418/proj/repo")
])
def test_get_repo_url(git_mock, url, expected):
    git_mock.return_value = url
    assert gitbump.get_repo_url() == expected
